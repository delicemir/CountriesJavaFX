package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import sample.Country.Country;
import javax.json.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public Button btnDownload;
    public ListView<String> countryListNames;
    public Label labelErrorInfo;
    public TextField txtJsonLink;
    public ImageView imageFlagOfCountry;
    public ComboBox comboCountries;
    public VBox vBoxCountryInfo;
    
    public TextField countryId;
    public TextField country;
    public TextField countryCapitalCityName;
    public TextField countryTelCode;
    public TextField countryWebLink;
    public Pane root;

    private String json="http://www.geognos.com/api/en/countries/info/all.json";
    private JsonObject jsonObjCountries;
    private JsonObject results;

    private ArrayList<Country> countries = new ArrayList<>();
    private ArrayList<String> countryNames = new ArrayList<>();

    private String countryName;
    private String capitalCityName;
    private String telephoneCountryCode;
    private String countryWebInfo;
    
    private String flagWebLink = "";
    ObservableList<String> data;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        startupInit();

        comboCountries.setOnAction(event -> {
            if(comboCountries.getValue()!=null) {
                int pos=0;
                pos=comboCountries.getSelectionModel().getSelectedIndex();
                showCountryInfo(pos);
            }
        });

        countryListNames.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                showCountryInfo(countryListNames.getSelectionModel().getSelectedIndex());
            }
        });

        btnDownload.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                countryNames.clear();
                countryListNames.getItems().clear();
                countries.clear();
                comboCountries.getItems().clear();

                if (!txtJsonLink.getText().isEmpty()) {
                    try {
                        json = readUrl(txtJsonLink.getText().toString());

                        JsonReader jsonReader = Json.createReader(new StringReader(json));
                        jsonObjCountries = jsonReader.readObject();
                        results = jsonObjCountries.getJsonObject("Results");

                        for (String key: results.keySet()) {
                            countryName=results.getJsonObject(key).getString("Name");
                            if(!results.getJsonObject(key).isNull("Capital")) {
                                capitalCityName = String.valueOf(results.getJsonObject(key).getJsonObject("Capital").get("Name"));
                            }
                            else {
                                capitalCityName="";
                            }
                            telephoneCountryCode = String.valueOf(results.getJsonObject(key).get("TelPref"));
                            countryWebInfo= String.valueOf(results.getJsonObject(key).get("CountryInfo"));
                            flagWebLink="http://www.geognos.com/api/en/countries/flag/"+key+".png";
                            countries.add(new Country(key,countryName,capitalCityName,telephoneCountryCode,countryWebInfo,flagWebLink));
                            
                        }
                        Collections.sort(countries);

                        for (Country c: countries) {
                            countryNames.add(c.getCountryName());
                        }

                        countryListNames.getItems().addAll(countryNames);
                        labelErrorInfo.setText("There are : "+countryNames.size()+" countries in the list!");

                        comboCountries.getItems().addAll(countryNames);

                        data = FXCollections.observableArrayList(countryNames);
                        countryListNames.setItems(data);
                        btnDownload.setDisable(true);
                        
                    } catch (Exception e) {
                        labelErrorInfo.setText(e.getMessage());
                    }
                }
                else {
                    labelErrorInfo.setText("JSON link can not be empty!");
                }

            }
        });

    }

    private void startupInit() {

        txtJsonLink.setText(json);
        imageFlagOfCountry.setImage(new Image("img/placeholder_flag.png"));
        imageFlagOfCountry.setFitHeight(160);
        imageFlagOfCountry.setPreserveRatio(true);
        imageFlagOfCountry.setSmooth(true);
        imageFlagOfCountry.setCache(true);

        vBoxCountryInfo.setPadding(new Insets(10,10,10,10));
        vBoxCountryInfo.setSpacing(5);

        comboCountries.setPromptText("Select country...");
        labelErrorInfo.setText("");
    }

    private void showCountryInfo(int pos) {

        if(!countries.isEmpty()) {
            countryId.setText(countries.get(pos).getCountryId());
            country.setText(countries.get(pos).getCountryName());
            countryCapitalCityName.setText(countries.get(pos).getCapitalCityName());
            countryTelCode.setText(countries.get(pos).getTelephoneCountryCode());
            countryWebLink.setText(countries.get(pos).getCountryWebInfo());

            flagWebLink = "http://www.geognos.com/api/en/countries/flag/" + countryId.getText().toString() + ".png";
            imageFlagOfCountry.setImage(new Image(flagWebLink));
        }
        else {
            labelErrorInfo.setText("Empty countries list!");
        }
    }

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } catch (Exception e){
            throw new Exception(e);
        }
        finally {
            if (reader != null)
                reader.close();
        }
    }

}
