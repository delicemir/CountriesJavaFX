package sample.Country;

public class Country implements Comparable<Country> {

    private String countryId;   // BA
    private String countryName;  // Bosnia and Herzegovina
    private String capitalCityName; // Sarajevo
    private String telephoneCountryCode;  // 387
    private String countryWebInfo;  // web link
    private String flagWebLink;  // link zastave

    public Country() {
    }

    public Country(String countryId, String countryName, String capitalCityName, String telephoneCountryCode, String countryWebInfo, String flagWebLink) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.capitalCityName = capitalCityName;
        this.telephoneCountryCode = telephoneCountryCode;
        this.countryWebInfo = countryWebInfo;
        this.flagWebLink = flagWebLink;
    }

    public String getCountryId() {
        return countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCapitalCityName() {
        return capitalCityName;
    }

    public String getTelephoneCountryCode() {
        return telephoneCountryCode;
    }

    public String getCountryWebInfo() {
        return countryWebInfo;
    }

    public String getFlagWebLink() {
        return flagWebLink;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryId='" + countryId + '\'' +
                ", countryName='" + countryName + '\'' +
                ", capitalCityName='" + capitalCityName + '\'' +
                ", telephoneCountryCode='" + telephoneCountryCode + '\'' +
                ", countryWebInfo='" + countryWebInfo + '\'' +
                ", flagWebLink='" + flagWebLink + '\'' +
                '}';
    }

    @Override
    public int compareTo(Country o) {
        return getCountryName().compareTo(o.getCountryName());
    }
}
